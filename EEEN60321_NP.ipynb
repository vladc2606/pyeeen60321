{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# EEEN60321 Power System Operation and Economics - Nodal Pricing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***&copy; 2021 Martínez Ceseña — University of Manchester, UK***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook provides several exercises covering nodal pricing and the effects of transmission networks.."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## List of contents"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Centralized or pool trading](#Centralized-or-pool-trading)\n",
    "  - [Separate markets](#Separate-markets)\n",
    "  - [Single market](#Single-market)\n",
    "- [Nodal prices](#Nodal-prices)\n",
    "  - [Three-bus system](#Three-bus-system)\n",
    "  - [Calculating the nodal prices](#Calculating-the-nodal-prices)\n",
    "- [Effect of losses](#Effect-of-losses)\n",
    "  - [Two-bus system - Linear costs](#Two-bus-system---Linear-costs)\n",
    "  - [Two-bus system - Quadratic costs](#Two-bus-system---Quadratic-costs)\n",
    "  - [Three-bus system](#Three-bus-system)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN60321-Power-System-Operation-and-Economics---Nodal-Pricing)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Before we begin"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we begin: \n",
    "- Make sure to review the asynchronous materials provided in blackboard for [EEEN60321&40321, Part 3, Lecture 02 - Nodal Pricing](https://online.manchester.ac.uk/webapps/blackboard/content/listContentEditable.jsp?content_id=_12837336_1&course_id=_67976_1)\n",
    "- The lecture builds on the concepts presented before in [EEEN60321&40321, Part 3, Lecture 01 - Optimal Power Flow & OPF Tutorial](https://online.manchester.ac.uk/webapps/blackboard/content/listContentEditable.jsp?content_id=_12837335_1&course_id=_67976_1), so make sure you have a good understanding of power flow (PF), economic dispatch (ED) and optimal power flow studies (OPF).\n",
    "- Check the [EEEN60321_OPF](./EEEN60321_OPF.ipynb) notebook which introuduces the tools used here.\n",
    "- If you have any questions, please post them in the discussion boards or, if that is not possible, send an email to alex.martinezcesena@manchester.ac.uk\n",
    "\n",
    "Before we begin, be aware that, to benefit the most from this notebook, you will need a basic understanding of: \n",
    "- [Linear programming](https://realpython.com/linear-programming-python/) (LP) models, which are the types of models presented below.\n",
    "- [Python](https://www.python.org/), which is the language used in this notebook.\n",
    "- The [pyomo](https://pyomo.readthedocs.io/en/stable/index.html) library, which is the optimisation software used to solve the examples in this notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will also need a few python libraries for this notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyomo.environ as pyo\n",
    "import numpy\n",
    "import ipywidgets as widgets\n",
    "from ipywidgets import interact"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As well as some tools that were developed in anothe notebook, i.e., EEEN60321_OPF."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Requirement already satisfied: nbimporter in c:\\users\\mbbx6ng3\\anaconda3\\lib\\site-packages (0.3.4)\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "WARNING: You are using pip version 20.0.2; however, version 21.3.1 is available.\n",
      "You should consider upgrading via the 'c:\\users\\mbbx6ng3\\anaconda3\\python.exe -m pip install --upgrade pip' command.\n"
     ]
    }
   ],
   "source": [
    "!pip install nbimporter"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "import nbimporter\n",
    "\n",
    "OPF = __import__('EEEN60321_OPF')\n",
    "get_LP = OPF.get_LP\n",
    "get_ED = OPF.get_ED\n",
    "print_LP = OPF.print_LP\n",
    "print_ED = OPF.print_ED\n",
    "get_QP_Direct = OPF.get_QP_Direct\n",
    "get_QP_Iterative = OPF.get_QP_Iterative\n",
    "get_ED_Iterative = OPF.get_ED_Iterative\n",
    "get_DCPF = OPF.get_DCPF\n",
    "Visualize_DC = OPF.Visualize_DC"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN60321-Power-System-Operation-and-Economics---Nodal-Pricing)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Centralized or pool trading"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To explore the differeces between centralized and pool trading options, consider the two areas presented below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![BorduriaSykdavia.png](Figures/BorduriaSykdavia.png \"Title\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the following costs:\n",
    "$$\n",
    "Cost_B = 0 + 10 G_B + 0.005 G_B^2 [$]\n",
    "$$\n",
    "\n",
    "$$\n",
    "Cost_S = 0 + 13 G_S + 0.01 G_S^2 [$]\n",
    "$$\n",
    "Which correspond to the following marginal costs:\n",
    "$$\n",
    "MC_B = 10 + 0.01 G_B [$/MWh]\n",
    "$$\n",
    "\n",
    "$$\n",
    "MC_S = 13 + 0.02 G_S [$/MWh]\n",
    "$$\n",
    "\n",
    "Also assume that the demand in Borduria is 500 MW, whereas it is 1500 MW in Syldavia"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN60321-Power-System-Operation-and-Economics---Nodal-Pricing)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Separate markets"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us begin with an ED for each area (e.g., separate markets) under the assumption that there is no trade between areas."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Cost = 6250.00 £\n",
      "Generation outputs:\n",
      "\tG1     =  500.00 \n",
      "Nodal prices:\n",
      "\tMC     =   15.00\n",
      "Iterations: 15\n",
      "R: 7500.00 [$/h]\n",
      "E: 7500.00 [$/h]\n"
     ]
    }
   ],
   "source": [
    "Generators = [\n",
    "    {'Bus':1, 'Min':0, 'Max':10000, 'Cost':[0, 10, 0.005]}\n",
    "]\n",
    "Loads = [\n",
    "    {'Bus': 1, 'Value': 500}\n",
    "]\n",
    "_, _, P, MC = get_ED_Iterative(Generators, Loads)\n",
    "R = [P[0]*MC]\n",
    "E = [P[0]*MC]\n",
    "print('R: %7.2f [$/h]'%(R[0]))\n",
    "print('E: %7.2f [$/h]'%(E[0]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Cost = 42000.00 £\n",
      "Generation outputs:\n",
      "\tG1     = 1500.00 \n",
      "Nodal prices:\n",
      "\tMC     =   43.00\n",
      "Iterations: 15\n",
      "R: 64500.00 [$/h]\n",
      "E: 64500.00 [$/h]\n"
     ]
    }
   ],
   "source": [
    "Generators = [\n",
    "    {'Bus':1, 'Min':0, 'Max':10000, 'Cost':[0, 13, 0.01]}\n",
    "]\n",
    "Loads = [\n",
    "    {'Bus': 1, 'Value': 1500}\n",
    "]\n",
    "_, _, P, MC = get_ED_Iterative(Generators, Loads)\n",
    "R.append(P[0]*MC)\n",
    "E.append(P[0]*MC)\n",
    "print('R: %7.2f [$/h]'%(R[1]))\n",
    "print('E: %7.2f [$/h]'%(E[1]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "R total: 72000.00 [$/h]\n",
      "E total: 72000.00 [$/h]\n"
     ]
    }
   ],
   "source": [
    "print('R total: %7.2f [$/h]'%sum(R))\n",
    "print('E total: %7.2f [$/h]'%sum(E))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN60321-Power-System-Operation-and-Economics---Nodal-Pricing)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Single market"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us now assume the areas can now exchange as much as needed (single market)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Cost = 35183.33 £\n",
      "Generation outputs:\n",
      "\tG1     = 1433.33 \n",
      "\tG2     =  566.67 \n",
      "Nodal prices:\n",
      "\tMC     =   24.33\n",
      "Iterations: 15\n",
      "R1:   34877.78 [$/h], R2:   13788.89 [$/h], R total:   48666.67 [$/h]\n",
      "E1:   12166.67 [$/h], E2:   36500.00 [$/h], E total:   48666.67 [$/h]\n"
     ]
    }
   ],
   "source": [
    "Generators = [\n",
    "    {'Bus':1, 'Min':0, 'Max':10000, 'Cost':[0, 10, 0.005]},\n",
    "    {'Bus':2, 'Min':0, 'Max':10000, 'Cost':[0, 13, 0.01]}\n",
    "]\n",
    "Loads = [\n",
    "    {'Bus': 1, 'Value': 500},\n",
    "    {'Bus': 2, 'Value': 1500}\n",
    "]\n",
    "_, _, P, MC = get_ED_Iterative(Generators, Loads)\n",
    "R1 = []\n",
    "E1 = []\n",
    "for p, load in zip(P, Loads):\n",
    "    R1.append(MC*p)\n",
    "    E1.append(MC*load['Value'])\n",
    "    \n",
    "print('R1: %10.2f [$/h], R2: %10.2f [$/h], R total: %10.2f [$/h]'%(R1[0], R1[1], sum(R1)))\n",
    "print('E1: %10.2f [$/h], E2: %10.2f [$/h], E total: %10.2f [$/h]'%(E1[0], E1[1], sum(E1)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that both nodal prices are the same as there are no line congestions.\n",
    "> What happens if the capacity of the interconnector is lower, e.g., 400 MW?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Cost = 39450.00 £\n",
      "Generation outputs:\n",
      "\tG1     =  900.00 \n",
      "\tG2     = 1100.00 \n",
      "Power flows:\n",
      "\tP1-2   =  400.00 (Binding constraint)\n",
      "Nodal prices:\n",
      "\tMCN1   =   19.00\n",
      "\tMCN2   =   35.00\n",
      "Marginal costs associated with the line capacities:\n",
      "\tMCL1-2 =   16.00\n",
      "Iterations: 15\n",
      "R1:   17100.00 [$/h], R2:   38499.99 [$/h], R total:   55599.99 [$/h]\n",
      "E1:    9500.00 [$/h], E2:   52499.99 [$/h], E total:   61999.99 [$/h]\n"
     ]
    }
   ],
   "source": [
    "Lines = [\n",
    "    {'From': 1, 'To': 2, 'X': 0.1, 'Capacity': 400}\n",
    "]\n",
    "model, results, P, MC = get_QP_Iterative(Generators, Lines, Loads)\n",
    "Set_Buses = range(len(P))\n",
    "R2 = [0 for x in Set_Buses]\n",
    "E2 = [0 for x in Set_Buses]\n",
    "for xb in Set_Buses:\n",
    "    R2[xb] = P[xb]*MC[xb]\n",
    "for load in Loads:\n",
    "    xb = load['Bus']-1\n",
    "    E2[xb] = load['Value']*MC[xb]\n",
    "print('R1: %10.2f [$/h], R2: %10.2f [$/h], R total: %10.2f [$/h]'%(R2[0], R2[1], sum(R2)))\n",
    "print('E1: %10.2f [$/h], E2: %10.2f [$/h], E total: %10.2f [$/h]'%(E2[0], E2[1], sum(E2)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now compare the produce revenues ($R$) and consumer payment ($E$) under the different conditions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "SEPARATE MARKETS\n",
      "R1:    7500.00 [$/h], R2:   64500.00 [$/h], R total:   72000.00 [$/h]\n",
      "E1:    7500.00 [$/h], E2:   64500.00 [$/h], E total:   72000.00 [$/h]\n",
      "\n",
      "SINGLE MARKET\n",
      "R1:   34877.78 [$/h], R2:   13788.89 [$/h], R total:   48666.67 [$/h]\n",
      "E1:   12166.67 [$/h], E2:   36500.00 [$/h], E total:   48666.67 [$/h]\n",
      "\n",
      "SINGLE MARKET WITH CONGESTION\n",
      "R1:   17100.00 [$/h], R2:   38499.99 [$/h], R total:   55599.99 [$/h]\n",
      "E1:    9500.00 [$/h], E2:   52499.99 [$/h], E total:   61999.99 [$/h]\n"
     ]
    }
   ],
   "source": [
    "print('SEPARATE MARKETS')\n",
    "print('R1: %10.2f [$/h], R2: %10.2f [$/h], R total: %10.2f [$/h]'%(R[0], R[1], sum(R)))\n",
    "print('E1: %10.2f [$/h], E2: %10.2f [$/h], E total: %10.2f [$/h]\\n'%(E[0], E[1], sum(E)))\n",
    "\n",
    "print('SINGLE MARKET')\n",
    "print('R1: %10.2f [$/h], R2: %10.2f [$/h], R total: %10.2f [$/h]'%(R1[0], R1[1], sum(R1)))\n",
    "print('E1: %10.2f [$/h], E2: %10.2f [$/h], E total: %10.2f [$/h]\\n'%(E1[0], E1[1], sum(E1)))\n",
    "\n",
    "print('SINGLE MARKET WITH CONGESTION')\n",
    "print('R1: %10.2f [$/h], R2: %10.2f [$/h], R total: %10.2f [$/h]'%(R2[0], R2[1], sum(R2)))\n",
    "print('E1: %10.2f [$/h], E2: %10.2f [$/h], E total: %10.2f [$/h]'%(E2[0], E2[1], sum(E2)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN60321-Power-System-Operation-and-Economics---Nodal-Pricing)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Nodal prices "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Three-bus system"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the thre-bus system provided below as a means to investigate the calculations of nodal prices."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Three_Bus_Complex_Example.png](Figures/Three_Bus_Complex_Example.png \"Title\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As before, we can begin with an ED, where all nodal prices will be the same and some of the line limits may be exceeded."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Cost = 2647.50 £\n",
      "Generation outputs:\n",
      "\tG1     =  125.00 \n",
      "\tG2     =  285.00 (Binding constraint)\n",
      "\tG3     =    0.00 \n",
      "\tG4     =    0.00 \n",
      "Nodal prices:\n",
      "\tMC     =    7.50\n"
     ]
    }
   ],
   "source": [
    "Generators = [\n",
    "    {'Bus':1, 'Min':0, 'Max':140, 'MC':7.5},\n",
    "    {'Bus':1, 'Min':0, 'Max':285, 'MC':6},\n",
    "    {'Bus':2, 'Min':0, 'Max':90, 'MC':14},\n",
    "    {'Bus':3, 'Min':0, 'Max':850, 'MC':10}\n",
    "]\n",
    "Loads = [\n",
    "    {'Bus': 1, 'Value': 50},\n",
    "    {'Bus': 2, 'Value': 60},\n",
    "    {'Bus': 3, 'Value': 300}\n",
    "]\n",
    "\n",
    "model, results = get_ED(Generators, Loads)\n",
    "print_ED(model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is possible to check if this solution is feasible using a power flow simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "NET POWER INJECTIONS:\n",
      " 1) 360.0000 MW\n",
      " 2) -60.0000 MW\n",
      " 3) -300.0000 MW\n",
      "POWER FLOWS:\n",
      " 1- 2) 156.0000 (Line constraint violation)\n",
      " 1- 3) 204.0000 \n",
      " 2- 3)  96.0000 \n",
      "\n"
     ]
    }
   ],
   "source": [
    "xg = 0\n",
    "for gen in Generators:\n",
    "    gen['P'] = model.Generation[xg].value\n",
    "    xg += 1\n",
    "Lines = [\n",
    "    {'From': 1, 'To': 2, 'X': 0.2, 'Capacity': 126},\n",
    "    {'From': 1, 'To': 3, 'X': 0.2, 'Capacity': 250},\n",
    "    {'From': 2, 'To': 3, 'X': 0.1, 'Capacity': 130}\n",
    "]\n",
    "𝜃, P = get_DCPF(Generators, Lines, Loads)\n",
    "Visualize_DC(Lines, 𝜃, P)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can address the network issues with an OPF."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Cost = 2835.00 £\n",
      "Generation outputs:\n",
      "\tG1     =   50.00 \n",
      "\tG2     =  285.00 (Binding constraint)\n",
      "\tG3     =    0.00 \n",
      "\tG4     =   75.00 \n",
      "Power flows:\n",
      "\tP1-2   =  126.00 (Binding constraint)\n",
      "\tP1-3   =  159.00 \n",
      "\tP2-3   =   66.00 \n",
      "Nodal prices:\n",
      "\tMCN1   =    7.50\n",
      "\tMCN2   =   11.25\n",
      "\tMCN3   =   10.00\n",
      "Marginal costs associated with the line capacities:\n",
      "\tMCL1-2 =    6.25\n",
      "\tMCL1-3 =    0.00\n",
      "\tMCL2-3 =    0.00\n"
     ]
    }
   ],
   "source": [
    "model, results = get_LP(Generators, Lines, Loads)\n",
    "print_LP(model)\n",
    "Cost = model.Objective_Function.expr()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Can you explain why the nodal prices are different now?\n",
    "- Can you calculate the nodal prices?\n",
    "- Can you calculate the marginal costs associated with the lines?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN60321-Power-System-Operation-and-Economics---Nodal-Pricing)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Calculating the nodal prices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Based on the results above we know that:\n",
    "- Generator B is operating at maximum capacity, so we can simplify its representation in the following equations, i.e., load is calculated as $125 = 50+60+300-285$\n",
    "- Generator C is not in use, so it can be ignored in the calculations\n",
    "- Generators A and D are the marginal generators with marginal costs of 7.5 $\\$$/MWh and 10 7.5 $\\$$/MWh, respectively\n",
    "- The capacity of line 1-2 is binding, this constraint will be managed by Generators A and D, i.e., $G_A \\frac{3}{5} + G_D \\frac{1}{5}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Based on the above, additional loads connected to bus 1 will be supplied by the cheapest generator:\n",
    "> The nodal price at bus 1 is 7.5 $\\$$/MWh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This can be validated by simulating the system after increasing the load in bus 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Cost = 2842.50 £\n",
      "Generation outputs:\n",
      "\tG1     =   51.00 \n",
      "\tG2     =  285.00 (Binding constraint)\n",
      "\tG3     =    0.00 \n",
      "\tG4     =   75.00 \n",
      "Power flows:\n",
      "\tP1-2   =  126.00 (Binding constraint)\n",
      "\tP1-3   =  159.00 \n",
      "\tP2-3   =   66.00 \n",
      "Nodal prices:\n",
      "\tMCN1   =    7.50\n",
      "\tMCN2   =   11.25\n",
      "\tMCN3   =   10.00\n",
      "Marginal costs associated with the line capacities:\n",
      "\tMCL1-2 =    6.25\n",
      "\tMCL1-3 =    0.00\n",
      "\tMCL2-3 =    0.00\n",
      "\n",
      "Calculated MC1:    7.50\n"
     ]
    }
   ],
   "source": [
    "Loads = [\n",
    "    {'Bus': 1, 'Value': 50+1},\n",
    "    {'Bus': 2, 'Value': 60},\n",
    "    {'Bus': 3, 'Value': 300}\n",
    "]\n",
    "model, results = get_LP(Generators, Lines, Loads)\n",
    "print_LP(model)\n",
    "Cost1 = model.Objective_Function.expr()\n",
    "print('\\nCalculated MC1: %7.2f'%(Cost1-Cost))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If load increases in bus3, it cannot be supplied by the cheapest generator as it would overload line 1-2, it could also be supplied by Generator C at a price of 14 $\\$$/MWh, but it is cheaper to use Generator D at a price of 10 $\\$$/MWh.\n",
    "> The nodal price at bus 3 is 10 $\\$$/MWh"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Cost = 2845.00 £\n",
      "Generation outputs:\n",
      "\tG1     =   50.00 \n",
      "\tG2     =  285.00 (Binding constraint)\n",
      "\tG3     =    0.00 \n",
      "\tG4     =   76.00 \n",
      "Power flows:\n",
      "\tP1-2   =  126.00 (Binding constraint)\n",
      "\tP1-3   =  159.00 \n",
      "\tP2-3   =   66.00 \n",
      "Nodal prices:\n",
      "\tMCN1   =    7.50\n",
      "\tMCN2   =   11.25\n",
      "\tMCN3   =   10.00\n",
      "Marginal costs associated with the line capacities:\n",
      "\tMCL1-2 =    6.25\n",
      "\tMCL1-3 =    0.00\n",
      "\tMCL2-3 =    0.00\n",
      "\n",
      "Calculated MC3:   10.00\n"
     ]
    }
   ],
   "source": [
    "Loads = [\n",
    "    {'Bus': 1, 'Value': 50},\n",
    "    {'Bus': 2, 'Value': 60},\n",
    "    {'Bus': 3, 'Value': 300+1}\n",
    "]\n",
    "model, results = get_LP(Generators, Lines, Loads)\n",
    "print_LP(model)\n",
    "Cost1 = model.Objective_Function.expr()\n",
    "print('\\nCalculated MC3: %7.2f'%(Cost1-Cost))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If load increases in bus 2, it can be supplied by Generator C at a price of 14 $\\$$/MWh. However, it can also be supplied through combinations of generators A and D, which has to be calculated.\n",
    "$$\n",
    "\\mathcal{L}(G_A, G_D) = (7.5 G_A + 10 G_D) + \\lambda (125 - G_A - G_D) + \\mu(G_A \\frac{3}{5} + G_D \\frac{1}{5} - 45) \n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we differentiate the Lagrangian function, we get this system of equations\n",
    "$$\n",
    "\\frac{\\partial \\mathcal{L}(G_A, G_D)}{\\partial G_A} = 7.5 - \\lambda + \\mu \\frac{3}{5} = 0\n",
    "$$\n",
    "\n",
    "$$\n",
    "\\frac{\\partial \\mathcal{L}(G_A, G_D)}{\\partial G_B} = 10 - \\lambda + \\mu \\frac{1}{5} = 0\n",
    "$$\n",
    "\n",
    "Which can be solved as follows:\n",
    "\n",
    "$$\n",
    " \\lambda = 7.5 + \\mu \\frac{3}{5} = 10 + \\mu \\frac{1}{5}\n",
    "$$\n",
    "\n",
    "$$\n",
    "\\mu \\frac{2}{5} = 2.5 \\therefore \\mu = 6.25 \\$/MWh\n",
    "$$\n",
    "\n",
    "$$\n",
    " \\lambda = 7.5 + 6.25 \\frac{3}{5} = 11.25 \\$/MWh\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> The nodal price at bus 2 is 11.25 $\\$$/MWh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN60321-Power-System-Operation-and-Economics---Nodal-Pricing)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Effect of losses"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To conclude this notebook, let us explore the impacts that losses can have on the power flows and nodal prices. The model used here is a fixed losses percentage applied to the flows across each line and then assigned to the buses connected to the lines (50% to each bus)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Two-bus system - Linear costs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first example to explore is the following two-bus system, and addume linear costs:\n",
    "![BorduriaSykdavia.png](Figures/BorduriaSykdavia.png \"Title\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "9ccfa47b51bf4becbb1e088a7360c80a",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=0.0, continuous_update=False, description='Losses (%)', max=5.0, step=…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact\n",
    "def Bus2(Loss = widgets.FloatSlider(min=0,max=5,step=1,value=0,description='Losses (%)', continuous_update=False),\n",
    "        Line = widgets.FloatSlider(min=0,max=600,step=10,value=200,description='Line (MW)', continuous_update=False)):\n",
    "    Generators = [\n",
    "        {'Bus':1, 'Min':0, 'Max':10000, 'MC':30},\n",
    "        {'Bus':2, 'Min':0, 'Max':10000, 'MC':13}\n",
    "    ]\n",
    "    Loads = [\n",
    "        {'Bus': 1, 'Value': 500},\n",
    "        {'Bus': 2, 'Value': 1500}\n",
    "    ]\n",
    "    Lines = [\n",
    "        {'From': 1, 'To': 2, 'X': 0.1, 'Capacity': Line}\n",
    "    ]\n",
    "    model, results = get_LP(Generators, Lines, Loads, Loss/100)\n",
    "    print_LP(model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN60321-Power-System-Operation-and-Economics---Nodal-Pricing)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Two-bus system - Quadratic costs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us continue with the same system, but now considering quadratic cost functions.\n",
    "![BorduriaSykdavia.png](Figures/BorduriaSykdavia.png \"Title\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "1b312aa7cfdf49398ee9990ef6a12977",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=0.0, continuous_update=False, description='Losses (%)', max=5.0, step=…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact\n",
    "def Bus2Q(Loss = widgets.FloatSlider(min=0,max=5,step=1,value=0,description='Losses (%)', continuous_update=False),\n",
    "        Line = widgets.FloatSlider(min=0,max=1000,step=10,value=400,description='Line (MW)', continuous_update=False)):\n",
    "    Generators = [\n",
    "        {'Bus':1, 'Min':0, 'Max':10000, 'Cost':[0, 10, 0.005]},\n",
    "        {'Bus':2, 'Min':0, 'Max':10000, 'Cost':[0, 13, 0.01]}\n",
    "    ]\n",
    "    Loads = [\n",
    "        {'Bus': 1, 'Value': 500},\n",
    "        {'Bus': 2, 'Value': 1500}\n",
    "    ]\n",
    "    Lines = [\n",
    "        {'From': 1, 'To': 2, 'X': 0.1, 'Capacity': Line}\n",
    "    ]\n",
    "    model, results, P, MC = get_QP_Iterative(Generators, Lines, Loads, Loss/100)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN60321-Power-System-Operation-and-Economics---Nodal-Pricing)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Three-bus system"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The last network to analyse is the three-bus system presented below:\n",
    "![Three_Bus_Complex_Example.png](Figures/Three_Bus_Complex_Example.png \"Title\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "0480e5a3da664c0399bad55d7bbb34be",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=0.0, continuous_update=False, description='Losses (%)', max=5.0, step=…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact\n",
    "def Bus2Q(Loss = widgets.FloatSlider(min=0,max=5,step=1,value=0,description='Losses (%)', continuous_update=False),\n",
    "          Line1 = widgets.FloatSlider(min=0,max=200,step=10,value=126,description='Line (MW)', continuous_update=False),\n",
    "          Line2 = widgets.FloatSlider(min=0,max=250,step=10,value=250,description='Line (MW)', continuous_update=False),\n",
    "          Line3 = widgets.FloatSlider(min=0,max=150,step=10,value=130,description='Line (MW)', continuous_update=False)):\n",
    "\n",
    "    Generators = [\n",
    "        {'Bus':1, 'Min':0, 'Max':140, 'MC':7.5},\n",
    "        {'Bus':1, 'Min':0, 'Max':285, 'MC':6},\n",
    "        {'Bus':2, 'Min':0, 'Max':90, 'MC':14},\n",
    "        {'Bus':3, 'Min':0, 'Max':850, 'MC':10}\n",
    "    ]\n",
    "    Loads = [\n",
    "        {'Bus': 1, 'Value': 50},\n",
    "        {'Bus': 2, 'Value': 60},\n",
    "        {'Bus': 3, 'Value': 300}\n",
    "    ]\n",
    "\n",
    "    Lines = [\n",
    "        {'From': 1, 'To': 2, 'X': 0.2, 'Capacity': Line1},\n",
    "        {'From': 1, 'To': 3, 'X': 0.2, 'Capacity': Line2},\n",
    "        {'From': 2, 'To': 3, 'X': 0.1, 'Capacity': Line3}\n",
    "    ]\n",
    "    model, results = get_LP(Generators, Lines, Loads, Loss/100)\n",
    "    print_LP(model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN60321-Power-System-Operation-and-Economics---Nodal-Pricing)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "165px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
