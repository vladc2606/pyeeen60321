{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# EEEN60321 Power System Operation and Economics - Optimal Power Flow Examples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***&copy; 2022 Martínez Ceseña — University of Manchester, UK***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook provides a linear programming formulation of the DC OPF, including relevant tools developed in python's `pyomo` with applications to a 3-bus example."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## List of contents"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Three-bus system under cold conditions](#Three-bus-system-under-cold-conditions)\n",
    "- [Three-bus system under hot conditions](#Three-bus-system-under-hot-conditions)\n",
    "- [Numerical approximation of marginal costs](#Numerical-approximation-of-marginal-costs)\n",
    "- [Power flow](#Power-flow)\n",
    "- [Impacts of each constraint](#Impacts-of-each-constraint)\n",
    "- [Quadratic DC OPF -  Direct method](#Quadratic-DC-OPF---Direct-method)\n",
    "- [Quadratic DC OPF -  Iterative method](#Quadratic-DC-OPF---Iterative-method)\n",
    "- [Create your own example](#Create-your-own-example)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN60321-Power-System-Operation-and-Economics---Optimal-Power-Flow-Examples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Before we begin"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we begin: \n",
    "- Make sure to review the asynchronous materials provided in blackboard for [EEEN60321&40321, Part 3, Lecture 01 - Optimal Power Flow & OPF Tutorial](https://online.manchester.ac.uk/webapps/blackboard/content/listContentEditable.jsp?content_id=_14067956_1&course_id=_73707_1)  \n",
    "- Check the [EEEN60321_OPF](./EEEN60321_OPF.ipynb) notebook which introuduces the tools used here.\n",
    "- If you have any questions, please post them in the discussion boards or, if that is not possible, send an email to alex.martinezcesena@manchester.ac.uk"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will also need a few python libraries for this notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyomo.environ as pyo\n",
    "import numpy\n",
    "import ipywidgets as widgets\n",
    "from ipywidgets import interact"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As well as some tools that were developed in another notebook, i.e., EEEN60321_OPF."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Requirement already satisfied: nbimporter in c:\\programdata\\anaconda3\\lib\\site-packages (0.3.4)\n"
     ]
    }
   ],
   "source": [
    "!pip install nbimporter"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "import nbimporter\n",
    "\n",
    "OPF = __import__('EEEN60321_OPF')\n",
    "get_LP = OPF.get_LP\n",
    "print_LP = OPF.print_LP\n",
    "get_Variables = OPF.get_Variables\n",
    "Objective_rule = OPF.Objective_rule\n",
    "DC_rule = OPF.DC_rule\n",
    "ED_rule = OPF.ED_rule\n",
    "Balance_rule = OPF.Balance_rule\n",
    "Min_Generation_rule = OPF.Min_Generation_rule\n",
    "Max_Generation_rule = OPF.Max_Generation_rule\n",
    "Positive_Line_rule = OPF.Positive_Line_rule\n",
    "Negative_Line_rule = OPF.Negative_Line_rule\n",
    "get_QP_Direct = OPF.get_QP_Direct\n",
    "get_QP_Iterative = OPF.get_QP_Iterative\n",
    "print_Generation = OPF.print_Generation\n",
    "get_DCPF = OPF.get_DCPF\n",
    "Visualize_DC = OPF.Visualize_DC"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is also important to check the lecture and tutorial on OPF, which also covers the examples that will be addressed in this notebook, which are presented below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN60321-Power-System-Operation-and-Economics---Optimal-Power-Flow-Examples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Three-bus system under cold conditions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that the optimisation model has been coded, it is possible to perform OPFs using the command below:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Three_Bus_Example_Cold.png](Figures/Three_Bus_Example_Cold.png \"Title\")\n",
    "<center><b>Figure 1. </b>Three-bus example - Cold conditions.</center>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Cost = 15500.00 £\n",
      "Generation outputs:\n",
      "\tG1     =  450.00 (Binding constraint)\n",
      "\tG2     =   50.00 \n",
      "Power flows:\n",
      "\tP1-2   =  133.33 \n",
      "\tP1-3   =  316.67 \n",
      "\tP2-3   =  183.33 \n",
      "Nodal prices:\n",
      "\tMCN1   =   40.00\n",
      "\tMCN2   =   40.00\n",
      "\tMCN3   =   40.00\n",
      "Marginal costs associated with the line capacities:\n",
      "\tMCL1-2 =    0.00\n",
      "\tMCL1-3 =    0.00\n",
      "\tMCL2-3 =    0.00\n"
     ]
    }
   ],
   "source": [
    "Generators = [\n",
    "    {'Bus':1, 'Min':0, 'Max':450, 'MC':30},\n",
    "    {'Bus':2, 'Min':0, 'Max':500, 'MC':40}\n",
    "]\n",
    "Lines = [\n",
    "    {'From': 1, 'To': 2, 'X': 0.1, 'Capacity': 400},\n",
    "    {'From': 1, 'To': 3, 'X': 0.1, 'Capacity': 400},\n",
    "    {'From': 2, 'To': 3, 'X': 0.1, 'Capacity': 400}\n",
    "]\n",
    "Loads = [\n",
    "    {'Bus': 3, 'Value': 500},\n",
    "]\n",
    "\n",
    "model, results = get_LP(Generators, Lines, Loads)\n",
    "print_LP(model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN60321-Power-System-Operation-and-Economics---Optimal-Power-Flow-Examples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Three-bus system under hot conditions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It should be relatively straighforward to change the case study by modfying the input data. Let us test this by performing an OPF under hot conditions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Three_Bus_Example_Hot.png](Figures/Three_Bus_Example_Hot.png \"Title\")\n",
    "<center><b>Figure 2. </b>Three-bus example - Hot conditions.</center>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Cost = 16000.00 £\n",
      "Generation outputs:\n",
      "\tG1     =  400.00 \n",
      "\tG2     =  100.00 \n",
      "Power flows:\n",
      "\tP1-2   =  100.00 \n",
      "\tP1-3   =  300.00 (Binding constraint)\n",
      "\tP2-3   =  200.00 \n",
      "Nodal prices:\n",
      "\tMCN1   =   30.00\n",
      "\tMCN2   =   40.00\n",
      "\tMCN3   =   50.00\n",
      "Marginal costs associated with the line capacities:\n",
      "\tMCL1-2 =    0.00\n",
      "\tMCL1-3 =   30.00\n",
      "\tMCL2-3 =    0.00\n"
     ]
    }
   ],
   "source": [
    "Generators = [\n",
    "    {'Bus':1, 'Min':0, 'Max':450, 'MC':30},\n",
    "    {'Bus':2, 'Min':0, 'Max':500, 'MC':40}\n",
    "]\n",
    "Lines = [\n",
    "    {'From': 1, 'To': 2, 'X': 0.1, 'Capacity': 300},\n",
    "    {'From': 1, 'To': 3, 'X': 0.1, 'Capacity': 300},\n",
    "    {'From': 2, 'To': 3, 'X': 0.1, 'Capacity': 300}\n",
    "]\n",
    "Loads = [\n",
    "    {'Bus': 3, 'Value': 500},\n",
    "]\n",
    "\n",
    "model, results = get_LP(Generators, Lines, Loads)\n",
    "print_LP(model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN60321-Power-System-Operation-and-Economics---Optimal-Power-Flow-Examples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Numerical approximation of marginal costs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have a tool to rapidly perform OPF, we can experiment with different calculations. For example, the marginal costs associated with a constraint can be calculated with the following appraoch:\n",
    "\n",
    "$$\n",
    "Marginal costs = \\frac{\\Delta Cost - Cost}{Delta}\n",
    "$$\n",
    "\n",
    "Where $Cost$ is the original costs estimated with the OPF, $\\Delta Cost$ are costs calculated after varying an OPF parameter (e.g., demand, line capacity, etc.) and $\\Delta$ is the increment on the OPF parameter\n",
    "\n",
    "For example, let us calculate the nodal prices by increasing the load in every bus (one bus at a time)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "MCN1 =   30.00\n",
      "MCN2 =   40.00\n",
      "MCN3 =   50.00\n"
     ]
    }
   ],
   "source": [
    "# Selecting delta\n",
    "Delta = 0.001\n",
    "\n",
    "# Selecting input data\n",
    "Generators = [\n",
    "    {'Bus':1, 'Min':0, 'Max':450, 'MC':30},\n",
    "    {'Bus':2, 'Min':0, 'Max':500, 'MC':40}\n",
    "]\n",
    "Lines = [\n",
    "    {'From': 1, 'To': 2, 'X': 0.1, 'Capacity': 300},\n",
    "    {'From': 1, 'To': 3, 'X': 0.1, 'Capacity': 300},\n",
    "    {'From': 2, 'To': 3, 'X': 0.1, 'Capacity': 300}\n",
    "]\n",
    "Loads = [\n",
    "    {'Bus': 3, 'Value': 500}\n",
    "]\n",
    "\n",
    "# Solving original OPF\n",
    "model, results = get_LP(Generators, Lines, Loads)\n",
    "OF = model.Objective_Function.expr()\n",
    "\n",
    "# Increasing demand in bus 1 and solving a new OPF\n",
    "Loads = [\n",
    "    {'Bus': 1, 'Value': Delta},\n",
    "    {'Bus': 3, 'Value': 500}\n",
    "]\n",
    "model, results = get_LP(Generators, Lines, Loads)\n",
    "OF1 = model.Objective_Function.expr()\n",
    "print('MCN1 = %7.2f'%((OF1-OF)/Delta))\n",
    "\n",
    "# Increasing demand in bus 2 and solving a new OPF\n",
    "Loads = [\n",
    "    {'Bus': 2, 'Value': Delta},\n",
    "    {'Bus': 3, 'Value': 500}\n",
    "]\n",
    "model, results = get_LP(Generators, Lines, Loads)\n",
    "OF2 = model.Objective_Function.expr()\n",
    "print('MCN2 = %7.2f'%((OF2-OF)/Delta))\n",
    "\n",
    "# Increasing demand in bus 3 and solving a new OPF\n",
    "Loads = [{'Bus': 3, 'Value': 500+Delta}]\n",
    "model, results = get_LP(Generators, Lines, Loads)\n",
    "OF3 = model.Objective_Function.expr()\n",
    "print('MCN3 = %7.2f'%((OF3-OF)/Delta))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Can you get the marginal costs associated with line capacities?\n",
    "\n",
    "\n",
    "> Can you get the marginal costs associated with generation capacities?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN60321-Power-System-Operation-and-Economics---Optimal-Power-Flow-Examples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Power flow"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us explore the use of power flow simulations for the system above.\n",
    "- How is this different from the OPF?\n",
    "- What is the role of the slack bus?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "fa8fbea72eac4019857e2edcb3589cf4",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=1.0, description='Slack', max=3.0, min=1.0, step=1.0), FloatSlider(val…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact\n",
    "def Bus3(Slack = widgets.FloatSlider(min=1,max=3,step=1,value=1,description='Slack'),\n",
    "        G1 = widgets.FloatSlider(min=0,max=500,step=1,value=200,description='G1'),\n",
    "        G2 = widgets.FloatSlider(min=0,max=500,step=1,value=300,description='G2'),\n",
    "        D = widgets.FloatSlider(min=0,max=500,step=1,value=500,description='L3')):\n",
    "    Generators = [\n",
    "        {'Bus':1, 'Min':0, 'Max':450, 'Cost':[400, -10, 0.1], 'P':G1},\n",
    "        {'Bus':2, 'Min':0, 'Max':500, 'Cost':[200, 12, 0.05], 'P':G2}\n",
    "    ]\n",
    "    Lines = [\n",
    "        {'From': 1, 'To': 2, 'X': 0.1, 'Capacity': 300},\n",
    "        {'From': 1, 'To': 3, 'X': 0.1, 'Capacity': 300},\n",
    "        {'From': 2, 'To': 3, 'X': 0.1, 'Capacity': 300}\n",
    "    ]\n",
    "    Loads = [\n",
    "        {'Bus': 3, 'Value': D},\n",
    "    ]\n",
    "    𝜃, P = get_DCPF(Generators, Lines, Loads, int(Slack-1))\n",
    "    Visualize_DC(Lines, 𝜃, P)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Impacts of each constraint"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us now investigate how the solution of the optimisation changes as constraints are added.For that purpose, let us first use the data for the hot case"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "LP = pyo.ConcreteModel()\n",
    "LP.dual = pyo.Suffix(direction=pyo.Suffix.IMPORT_EXPORT)\n",
    "\n",
    "LP.Generators = [\n",
    "    {'Bus':1, 'Min':0, 'Max':450, 'MC':30},\n",
    "    {'Bus':2, 'Min':0, 'Max':500, 'MC':40}\n",
    "]\n",
    "LP.Lines = [\n",
    "    {'From': 1, 'To': 2, 'X': 0.1, 'Capacity': 300},\n",
    "    {'From': 1, 'To': 3, 'X': 0.1, 'Capacity': 300},\n",
    "    {'From': 2, 'To': 3, 'X': 0.1, 'Capacity': 300}\n",
    "]\n",
    "LP.Loads = [\n",
    "    {'Bus': 3, 'Value': 500},\n",
    "]\n",
    "LP.Loss=0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us first solve the problem without any of the constrants. In such case, as the model is trying to minimise costs, it will set the output of both generators to zero."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![LP_Search_Space.png](Figures/LP_Search_Space.png \"Title\")\n",
    "<center><b>Figure 3. </b>Search space.</center>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "WARNING: Empty constraint block written in LP format - solver may error\n",
      "Generation outputs:\n",
      "\tG1     =    0.00 \n",
      "\tG2     =    0.00 \n"
     ]
    }
   ],
   "source": [
    "get_Variables(LP)\n",
    "LP.Objective_Function = pyo.Objective(rule=Objective_rule)\n",
    "results = pyo.SolverFactory('glpk').solve(LP)\n",
    "print_Generation(LP)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, let us add the nodal balance constraints."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![LP_Nodal_Balance.png](Figures/LP_Nodal_Balance.png \"Title\")\n",
    "<center><b>Figure 4. </b>Adding nodal balance constraints.</center>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Generation outputs:\n",
      "\tG1     =  500.00 \n",
      "\tG2     =    0.00 \n"
     ]
    }
   ],
   "source": [
    "model.Set_ED = range(1)\n",
    "LP.Constraint_ED = pyo.Constraint(model.Set_ED, rule=ED_rule)\n",
    "results = pyo.SolverFactory('glpk').solve(LP)\n",
    "print_Generation(LP)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, let us include the capacity limits of the generators, this is equivalent to the ED formulation. Now, as the cheapest generator does not have sufficient capacity to meet the full demand, the expensive generator needs to cover the outstanding demand."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![LP_Generation_Constraints.png](Figures/LP_Generation_Constraints.png \"Title\")\n",
    "<center><b>Figure 5. </b>Adding generation capacity constraints.</center>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Generation outputs:\n",
      "\tG1     =  450.00 (Binding constraint)\n",
      "\tG2     =   50.00 \n"
     ]
    }
   ],
   "source": [
    "LP.Constraint_Min_Generation = pyo.Constraint(LP.Set_Generators, rule=Min_Generation_rule)\n",
    "LP.Constraint_Max_Generation = pyo.Constraint(LP.Set_Generators, rule=Max_Generation_rule)\n",
    "results = pyo.SolverFactory('glpk').solve(LP)\n",
    "print_Generation(LP)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we add the the DC model and line limits.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![LP_Line_Constraints.png](Figures/LP_Line_Constraints.png \"Title\")\n",
    "<center><b>Figure 6. </b>Adding line capacity constraints.</center>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Cost = 16000.00 £\n",
      "Generation outputs:\n",
      "\tG1     =  400.00 \n",
      "\tG2     =  100.00 \n",
      "Power flows:\n",
      "\tP1-2   =  100.00 \n",
      "\tP1-3   =  300.00 (Binding constraint)\n",
      "\tP2-3   =  200.00 \n",
      "Nodal prices:\n",
      "\tMCN1   =   -5.00\n",
      "\tMCN2   =    5.00\n",
      "\tMCN3   =   15.00\n",
      "Marginal costs associated with the line capacities:\n",
      "\tMCL1-2 =    0.00\n",
      "\tMCL1-3 =   30.00\n",
      "\tMCL2-3 =    0.00\n"
     ]
    }
   ],
   "source": [
    "LP.Constraint_DC = pyo.Constraint(LP.Set_Lines, rule=DC_rule)\n",
    "LP.Constraint_Balance = pyo.Constraint(LP.Set_Buses, rule=Balance_rule)\n",
    "LP.Constraint_Positive_Line = pyo.Constraint(LP.Set_Lines, rule=Positive_Line_rule)\n",
    "LP.Constraint_Negative_Line = pyo.Constraint(LP.Set_Lines, rule=Negative_Line_rule)\n",
    "results = pyo.SolverFactory('glpk').solve(LP)\n",
    "print_LP(LP)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN60321-Power-System-Operation-and-Economics---Optimal-Power-Flow-Examples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Quadratic DC OPF - Direct method"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check how the number of lines used to approximate the quadratic cost functions affect the results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "b17f7d76fbb1436fbc809305fc12e4f1",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=100.0, continuous_update=False, description='Pieces', min=2.0, step=1.…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact\n",
    "def Bus3(N = widgets.FloatSlider(min=2,max=100,step=1,value=100,description='Pieces', continuous_update=False)):\n",
    "    Generators = [\n",
    "        {'Bus':1, 'Min':0, 'Max':450, 'Cost':[400, -10, 0.1]},\n",
    "        {'Bus':2, 'Min':0, 'Max':500, 'Cost':[200, 12, 0.05]}\n",
    "    ]\n",
    "    Lines = [\n",
    "        {'From': 1, 'To': 2, 'X': 0.1, 'Capacity': 300},\n",
    "        {'From': 1, 'To': 3, 'X': 0.1, 'Capacity': 300},\n",
    "        {'From': 2, 'To': 3, 'X': 0.1, 'Capacity': 300}\n",
    "    ]\n",
    "    Loads = [\n",
    "        {'Bus': 3, 'Value': 500},\n",
    "    ]\n",
    "\n",
    "    model, results = get_QP_Direct(Generators, Lines, Loads, 0, int(N))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN60321-Power-System-Operation-and-Economics---Optimal-Power-Flow-Examples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Quadratic DC OPF - Iterative method"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us now check how the number of lines affectes the iterative approach."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "253cc2d9e6114d78bddb7f38ab8b7717",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=100.0, continuous_update=False, description='Pieces', min=2.0, step=1.…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact\n",
    "def Bus3(N = widgets.FloatSlider(min=2,max=100,step=1,value=100,description='Pieces', continuous_update=False)):\n",
    "    Generators = [\n",
    "        {'Bus':1, 'Min':0, 'Max':450, 'Cost':[400, -10, 0.1]},\n",
    "        {'Bus':2, 'Min':0, 'Max':500, 'Cost':[200, 12, 0.05]}\n",
    "    ]\n",
    "    Lines = [\n",
    "        {'From': 1, 'To': 2, 'X': 0.1, 'Capacity': 300},\n",
    "        {'From': 1, 'To': 3, 'X': 0.1, 'Capacity': 300},\n",
    "        {'From': 2, 'To': 3, 'X': 0.1, 'Capacity': 300}\n",
    "    ]\n",
    "    Loads = [\n",
    "        {'Bus': 3, 'Value': 500},\n",
    "    ]\n",
    "\n",
    "    model, results, Gen, MC = get_QP_Iterative(Generators, Lines, Loads, 0, int(N))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN60321-Power-System-Operation-and-Economics---Optimal-Power-Flow-Examples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create your own example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use the tools to develop your own examples. Consider:\n",
    "- Different network configurations\n",
    "- More generations in different locations\n",
    "- More loads in different locations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Cost = 16000.00 £\n",
      "Generation outputs:\n",
      "\tG1     =  400.00 \n",
      "\tG2     =  100.00 \n",
      "Power flows:\n",
      "\tP1-2   =  100.00 \n",
      "\tP1-3   =  300.00 (Binding constraint)\n",
      "\tP2-3   =  200.00 \n",
      "Nodal prices:\n",
      "\tMCN1   =   30.00\n",
      "\tMCN2   =   40.00\n",
      "\tMCN3   =   50.00\n",
      "Marginal costs associated with the line capacities:\n",
      "\tMCL1-2 =    0.00\n",
      "\tMCL1-3 =   30.00\n",
      "\tMCL2-3 =    0.00\n"
     ]
    }
   ],
   "source": [
    "Generators = [\n",
    "    {'Bus':1, 'Min':0, 'Max':450, 'MC':30},\n",
    "    {'Bus':2, 'Min':0, 'Max':500, 'MC':40}\n",
    "]\n",
    "Lines = [\n",
    "    {'From': 1, 'To': 2, 'X': 0.1, 'Capacity': 300},\n",
    "    {'From': 1, 'To': 3, 'X': 0.1, 'Capacity': 300},\n",
    "    {'From': 2, 'To': 3, 'X': 0.1, 'Capacity': 300}\n",
    "]\n",
    "Loads = [\n",
    "    {'Bus': 3, 'Value': 500},\n",
    "]\n",
    "\n",
    "model, results = get_LP(Generators, Lines, Loads)\n",
    "print_LP(model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN60321-Power-System-Operation-and-Economics---Optimal-Power-Flow-Examples)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "165px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
